from django.contrib.auth import get_user_model
import graphene
from shirts import models
from shirts.graphene import types


class CreateAttribute(graphene.Mutation):
    attribute_id = graphene.Int()
    name = graphene.String()

    class Arguments:
        name = graphene.String()

    def mutate(self, info, name):
        attribute = models.Attribute(name=name)
        attribute.save()

        return CreateAttribute(
            attribute_id=attribute.attribute_id,
            name=attribute.name,
        )


class CreateUser(graphene.Mutation):
    user = graphene.Field(types.UserType)

    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email = graphene.String(required=True)

    def mutate(self, info, username, password, email):
        user = get_user_model()(
            username=username,
            email=email,
        )
        user.set_password(password)
        user.save()

        return CreateUser(user=user)
