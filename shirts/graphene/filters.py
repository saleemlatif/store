import django_filters
from shirts import models


class AttributeFilter(django_filters.FilterSet):
    class Meta:
        model = models.Attribute
        fields = ['name']


class AttributeValueFilter(django_filters.FilterSet):
    class Meta:
        model = models.AttributeValue
        fields = ['attribute_id', 'value']


class AuditFilter(django_filters.FilterSet):
    class Meta:
        model = models.Audit
        fields = ['code', 'order_id']


class CategoryFilter(django_filters.FilterSet):
    class Meta:
        model = models.Category
        fields = ['name', 'department_id']


class CustomerFilter(django_filters.FilterSet):
    class Meta:
        model = models.Customer
        fields = ['name', 'city', 'country']


class DepartmentFilter(django_filters.FilterSet):
    class Meta:
        model = models.Department
        fields = ['name']


class OrderDetailFilter(django_filters.FilterSet):
    class Meta:
        model = models.OrderDetail
        fields = ['order_id', ]


class OrdersFilter(django_filters.FilterSet):
    class Meta:
        model = models.Orders
        fields = ['customer_id', ]


class ProductFilter(django_filters.FilterSet):
    class Meta:
        model = models.Product
        fields = ['name', ]


class ReviewFilter(django_filters.FilterSet):
    class Meta:
        model = models.Review
        fields = ['product_id', 'customer_id', 'rating']


class ShippingFilter(django_filters.FilterSet):
    class Meta:
        model = models.Shipping
        fields = ['shipping_type', 'shipping_cost', 'rating']


class ShippingRegionFilter(django_filters.FilterSet):
    class Meta:
        model = models.ShippingRegion
        fields = ['shipping_region']


class ShoppingCartFilter(django_filters.FilterSet):
    class Meta:
        model = models.ShoppingCart
        fields = ['cart_id', 'product_id']


class TaxFilter(django_filters.FilterSet):
    class Meta:
        model = models.Tax
        fields = ['tax_type', 'tax_percentage']
