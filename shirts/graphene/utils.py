
def filter_qs(qs, first=None, skip=None):
    """
    Filter the given queryset `qs`, include first `first` records or skip `skip` records or both.

    Arguments:
        qs (QuerySet): Django ORM query set object to filter.
        first (int | None): Number that denotes the count of records from the start of query set tp return.
        skip (int | None): Number that denotes the count of records to skip from the start of query set.

    Returns:
        (QuerySet): Django ORM query set object to filter.
    """
    if first:
        qs = qs[:first]
    if skip:
        qs = qs[skip:]

    return qs
