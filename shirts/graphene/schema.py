from django.contrib.auth import get_user_model
import graphene
from graphene_django.filter import DjangoFilterConnectionField

from shirts.graphene import types
from shirts.graphene import mutations
from shirts.graphene import utils
from shirts.graphene import filters


class Query(graphene.ObjectType):
    attributes = DjangoFilterConnectionField(types.AttributeType, filterset_class=filters.AttributeFilter)
    products = DjangoFilterConnectionField(types.ProductType, filterset_class=filters.ProductFilter)
    customers = DjangoFilterConnectionField(types.CustomerType, filterset_class=filters.CustomerFilter)

    users = graphene.List(types.UserType, first=graphene.Int(), skip=graphene.Int())
    me = graphene.Field(types.UserType)

    def resolve_users(self, info, first=None, skip=None, **kwargs):
        qs = get_user_model().objects.all()
        return utils.filter_qs(qs, first, skip)

    def resolve_me(self, info, **kwargs):
        user = info.context.user

        if user.is_anonymous:
            raise Exception('User Not logged in.')

        return user


class Mutation(graphene.ObjectType):
    create_attribute = mutations.CreateAttribute.Field()
    create_user = mutations.CreateUser.Field()
