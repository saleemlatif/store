import graphene
from django.contrib.auth import get_user_model
from graphene_django import DjangoObjectType

from shirts import models


class AttributeType(DjangoObjectType):
    class Meta:
        model = models.Attribute
        interfaces = (graphene.relay.Node,)


class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()
        interfaces = (graphene.relay.Node,)


class AttributeValueType(DjangoObjectType):
    class Meta:
        model = models.AttributeValue
        interfaces = (graphene.relay.Node,)


class AuditType(DjangoObjectType):
    class Meta:
        model = models.Audit
        interfaces = (graphene.relay.Node,)


class CategoryType(DjangoObjectType):
    class Meta:
        model = models.Category
        interfaces = (graphene.relay.Node,)


class CustomerType(DjangoObjectType):
    class Meta:
        model = models.Customer
        interfaces = (graphene.relay.Node,)


class DepartmentType(DjangoObjectType):
    class Meta:
        model = models.Department
        interfaces = (graphene.relay.Node,)


class OrderDetailType(DjangoObjectType):
    class Meta:
        model = models.OrderDetail
        interfaces = (graphene.relay.Node,)


class OrdersType(DjangoObjectType):
    class Meta:
        model = models.Orders
        interfaces = (graphene.relay.Node,)


class ProductType(DjangoObjectType):
    class Meta:
        model = models.Product
        interfaces = (graphene.relay.Node,)


class ReviewType(DjangoObjectType):
    class Meta:
        model = models.Review
        interfaces = (graphene.relay.Node,)


class ShippingType(DjangoObjectType):
    class Meta:
        model = models.Shipping
        interfaces = (graphene.relay.Node,)


class ShippingRegionType(DjangoObjectType):
    class Meta:
        model = models.ShippingRegion
        interfaces = (graphene.relay.Node,)


class ShoppingCartType(DjangoObjectType):
    class Meta:
        model = models.ShoppingCart
        interfaces = (graphene.relay.Node,)


class TaxType(DjangoObjectType):
    class Meta:
        model = models.Tax
        interfaces = (graphene.relay.Node,)
