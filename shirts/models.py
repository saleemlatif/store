from django.db import models


class ReadableObjectMixin:
    """
    This Mixin adds implementation of str and repr to make object of the class readable.
    """
    extra_repr_fields = tuple()

    def __repr__(self):
        return "<{self.__class__.__name__} {self.pk}: {fields!r}>".format(
            self=self,
            fields=": ".join(repr(getattr(self, field)) for field in self.extra_repr_fields)
        )

    def __str__(self):
        return repr(self)


class BaseModel(ReadableObjectMixin, models.Model):
    class Meta:
        abstract = True


class Attribute(BaseModel):
    attribute_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'attribute'

    extra_repr_fields = ('name', )


class AttributeValue(BaseModel):
    attribute_value_id = models.AutoField(primary_key=True)
    attribute = models.ForeignKey(
        'Attribute', db_column='attribute_id', on_delete=models.CASCADE, related_name='attribute_value'
    )
    value = models.CharField(max_length=100)

    class Meta:
        db_table = 'attribute_value'

    extra_repr_fields = ('value', )


class Audit(BaseModel):
    audit_id = models.AutoField(primary_key=True)
    order = models.ForeignKey('Orders', db_column='order_id', on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    message = models.TextField()
    code = models.IntegerField()

    class Meta:
        db_table = 'audit'

    extra_repr_fields = ('code', )


class Category(BaseModel):
    category_id = models.AutoField(primary_key=True)
    department = models.ForeignKey(
        "Department", db_column='department_id', on_delete=models.CASCADE, related_name='categories'
    )
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        db_table = 'category'
        verbose_name_plural = 'Categories'

    extra_repr_fields = ('name',)


class Customer(BaseModel):
    customer_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    credit_card = models.TextField(blank=True, null=True)
    address_1 = models.CharField(max_length=100, blank=True, null=True)
    address_2 = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    region = models.CharField(max_length=100, blank=True, null=True)
    postal_code = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    shipping_region = models.ForeignKey('ShippingRegion', db_column='shipping_region_id', on_delete=models.SET(1))
    day_phone = models.CharField(max_length=100, blank=True, null=True)
    eve_phone = models.CharField(max_length=100, blank=True, null=True)
    mob_phone = models.CharField(max_length=100, blank=True, null=True)

    user = models.OneToOneField('auth.User', related_name='customer', on_delete=models.CASCADE)

    class Meta:
        db_table = 'customer'

    extra_repr_fields = ('name',)


class Department(BaseModel):
    department_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        db_table = 'department'

    extra_repr_fields = ('name',)


class OrderDetail(BaseModel):
    item_id = models.AutoField(primary_key=True)
    order = models.ForeignKey('Orders', db_column='order_id', on_delete=models.CASCADE)
    product = models.ForeignKey('Product', db_column='product_id', on_delete=models.CASCADE)
    attributes = models.CharField(max_length=1000)
    product_name = models.CharField(max_length=100)
    quantity = models.IntegerField()
    unit_cost = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = 'order_detail'

    extra_repr_fields = ('product_name',)


class Orders(BaseModel):
    order_id = models.AutoField(primary_key=True)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2)
    created_on = models.DateTimeField(auto_now_add=True)
    shipped_on = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField()
    comments = models.CharField(max_length=255, blank=True, null=True)
    customer = models.ForeignKey(
        'Customer', db_column='customer_id', on_delete=models.SET_NULL, blank=True, null=True
    )
    auth_code = models.CharField(max_length=50, blank=True, null=True)
    reference = models.CharField(max_length=50, blank=True, null=True)
    shipping_id = models.IntegerField(blank=True, null=True)
    tax = models.ForeignKey(
        'Tax', db_column='tax_id', on_delete=models.SET_NULL, blank=True, null=True
    )

    class Meta:
        db_table = 'orders'
        verbose_name_plural = 'Orders'

    extra_repr_fields = ('status',)


class Product(BaseModel):
    product_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    discounted_price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.CharField(max_length=150, blank=True, null=True)
    image_2 = models.CharField(max_length=150, blank=True, null=True)
    thumbnail = models.CharField(max_length=150, blank=True, null=True)
    display = models.SmallIntegerField()

    categories = models.ManyToManyField('Category', through='ProductCategory', related_name='products')
    attribute_values = models.ManyToManyField('AttributeValue', through='ProductAttribute', related_name='products')

    class Meta:
        db_table = 'product'

    extra_repr_fields = ('name',)


class ProductAttribute(BaseModel):
    product_id = models.ForeignKey('Product', db_column='product_id', primary_key=True, on_delete=models.CASCADE)
    attribute_value_id = models.ForeignKey('AttributeValue', db_column='attribute_value_id', on_delete=models.CASCADE)

    class Meta:
        db_table = 'product_attribute'
        unique_together = (('product_id', 'attribute_value_id'),)

    extra_repr_fields = ('attribute_value_id',)


class ProductCategory(BaseModel):
    product_id = models.ForeignKey('Product', db_column='product_id', primary_key=True, on_delete=models.CASCADE)
    category_id = models.ForeignKey('Category', db_column='category_id', on_delete=models.CASCADE)

    class Meta:
        db_table = 'product_category'
        unique_together = (('product_id', 'category_id'),)

    extra_repr_fields = ('category_id',)


class Review(BaseModel):
    review_id = models.AutoField(primary_key=True)
    customer = models.ForeignKey('Customer', db_column='customer_id', on_delete=models.CASCADE)
    product = models.ForeignKey('Product', db_column='product_id', on_delete=models.CASCADE)
    review = models.TextField()
    rating = models.SmallIntegerField()
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'review'

    extra_repr_fields = ('rating',)


class Shipping(BaseModel):
    shipping_id = models.AutoField(primary_key=True)
    shipping_type = models.CharField(max_length=100)
    shipping_cost = models.DecimalField(max_digits=10, decimal_places=2)
    shipping_region = models.ForeignKey('ShippingRegion', db_column='shipping_region_id', on_delete=models.CASCADE)

    class Meta:
        db_table = 'shipping'

    extra_repr_fields = ('shipping_type',)


class ShippingRegion(BaseModel):
    shipping_region_id = models.AutoField(primary_key=True)
    shipping_region = models.CharField(max_length=100)

    class Meta:
        db_table = 'shipping_region'

    extra_repr_fields = ('shipping_region',)


class ShoppingCart(BaseModel):
    item_id = models.AutoField(primary_key=True)
    cart_id = models.CharField(max_length=32)
    product = models.ForeignKey('Product', db_column='product_id', on_delete=models.CASCADE)
    attributes = models.CharField(max_length=1000)
    quantity = models.IntegerField()
    buy_now = models.IntegerField()
    added_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'shopping_cart'

    extra_repr_fields = ('product',)


class Tax(BaseModel):
    tax_id = models.AutoField(primary_key=True)
    tax_type = models.CharField(max_length=100)
    tax_percentage = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = 'tax'

    extra_repr_fields = ('tax_type', 'tax_percentage')
