from django.contrib import admin

from shirts import models


@admin.register(models.Attribute)
class AttributeAdmin(admin.ModelAdmin):
    list_display = ('attribute_id', 'name')


@admin.register(models.AttributeValue)
class AttributeValueAdmin(admin.ModelAdmin):
    list_display = ('attribute_value_id', 'attribute', 'value')


@admin.register(models.Audit)
class AuditAdmin(admin.ModelAdmin):
    list_display = ('audit_id', 'order', 'created_on', 'code')


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('category_id', 'department', 'name')


@admin.register(models.Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('customer_id', 'name', 'user', 'city', 'country')


@admin.register(models.Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('department_id', 'name')


@admin.register(models.OrderDetail)
class OrderDetailAdmin(admin.ModelAdmin):
    list_display = ('item_id', 'order', 'product', 'product_name', 'quantity', 'unit_cost')


@admin.register(models.Orders)
class OrdersAdmin(admin.ModelAdmin):
    list_display = ('order_id', 'total_amount', 'status', 'customer', 'created_on', 'shipped_on')


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('product_id', 'name', 'price', 'discounted_price')


@admin.register(models.Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ('review_id', 'customer', 'product', 'rating', 'created_on')


@admin.register(models.Shipping)
class ShippingAdmin(admin.ModelAdmin):
    list_display = ('shipping_id', 'shipping_type', 'shipping_cost', 'shipping_region')


@admin.register(models.ShippingRegion)
class ShippingRegionAdmin(admin.ModelAdmin):
    list_display = ('shipping_region_id', 'shipping_region')


@admin.register(models.ShoppingCart)
class ShoppingCartAdmin(admin.ModelAdmin):
    list_display = ('item_id', 'cart_id', 'product', 'quantity', 'added_on')


@admin.register(models.Tax)
class TaxAdmin(admin.ModelAdmin):
    list_display = ('tax_id', 'tax_type', 'tax_percentage')

