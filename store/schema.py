import graphene
import graphql_jwt

from shirts.graphene import schema as shirts_schema


class Query(shirts_schema.Query, graphene.ObjectType):
    pass


class Mutation(shirts_schema.Mutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
