FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir -p /store/code/ /store/logs/ /store/var/
WORKDIR /store/code/
COPY . /store/code/
RUN chmod 0777 /store/code/scripts/wait-for
RUN make setup_docker
