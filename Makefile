.PHONY: requirements
.DEFAULT_GOAL := help


# Generates a help message. Borrowed from https://github.com/pydanny/cookiecutter-djangopackage.
help: ## Display this help message
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-25s\033[0m %s\n", $$1, $$2}'

requirements: ## Install requirements
	pip install -r requirements/base.txt

dev.up:
	docker-compose up

migrate:
	python manage.py migrate --no-input

setup_docker: requirements

serve: migrate
	python manage.py runserver 0.0.0.0:8000


destroy:
	docker-compose down -v
	docker-compose build --no-cache

rebuild: destroy dev.up
